# 基于uniapp的奶茶店点餐微信小程序+基于SpringBoot和Vue的管理后台

### 在线演示
- h5, 微信打开链接: https://xxl.today/naicha_ui 测试号:123
- 小程序, 只能本地演示
- 商家后台, http://xxl.today/naicha_admin_ui 账号:system 密码:system

```text
2套模式，小程序和h5页面，都支持
h5可以配置公众号模式
小程序就直接小程序
```

### 小程序截图
![小程序截图.png](截图/小程序截图.png)

### 管理后台截图
![小程序截图.png](截图/闰土刺茶管理后台截图.png)
